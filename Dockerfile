FROM openjdk:8

COPY  simple-java-maven-app/target/*.jar  myapp.jar

ENTRYPOINT  [ "java" , "-jar" ,  "myapp.jar"  ]
